<# Start this script with this to add arguments like a cmdlet
[CmdletBinding()]
param(
    [Parameter(Mandatory=$true)]
    [string]$lol
)
#>

#--------------Constants-------------------
$NuspecFilePath = "$PSScriptRoot\RomisfragPowershellToolbox.nuspec"
$OutputDirectory = "$PSScriptRoot\Outputs"


#-------------Dependencies verifications------------

if($(Get-Command "nuget" 2>$null) -eq $null){
    Write-Error "Fatal Error : You have to install nuget to use this script"
    exit(1)
}

if(-not (Test-Path $NuspecFilePath)){
    Write-Error "Fatal Error : $NuspecFilePath does not exist in your directory, you should try to pull latest version"
    exit(1)
}


#------------Packing the application--------------
nuget pack $NuspecFilePath -OutputDirectory $OutputDirectory

<#
Then you can push this using 
nuget push .\Outputs\RomisfragPowershellToolbox.1.0.0.nupkg -Source RomisfragPowershellToolbox
#>
