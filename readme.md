# Installation

### First add the source to nuget

`nuget source Add -Name 'RomisfragPowershellToolboxFeed' -Source "https://gitlab.com/api/v4/projects/38282722/packages/nuget/index.json"`

### Then everytime you wan't to install a package, just use

`nuget install RomisfragPowershellToolbox -Source "RomisfragPowershellToolboxFeed"`


### If you have troubles with nuget cache
`nuget locals all -clear`

# Improvement with powershellGet version 3.0.0 or greater

## Installation of powershellGet > 3.0.0
PowershellGet 3.0 comes with a new set of fonctionality that allow you to install nuget package from various sources including gitlab nuget repository
To install PowershellGet 3.0 please visit https://www.powershellgallery.com/packages/PowerShellGet/ and select at the bottom a version that is higher than 3.0.0

Manual download the nupkg file

Rename the downloaded .nupkg file in .zip

Execute this command to get the location of your already installed powershellGet

`Get-Module PowershellGet | FL`

You should get an output like this

`<your filesystem>\PowershellGet\<version number>\PSModule.psm1`

So move to the PowershellGet above folder and create a folder with the name of the PowershellGet version number you have downloaded (you can have the exact version number to set inside the PowerShellGet.psd1 file inside the zip archive you have downloaded)

Unzip all the content of the zip inside this new folder and then use this command : 

`Import-Module PowerShellGet -Version <version you have downloaded>`

When running this command 
`Get-Command -Module PowershellGet`
If you still get some output of CmdLet with the old version number of PowershellGet, then run : 

`Remove-Module PowershellGet`

And now you should only have the new PowershellGet commands

# Now enjoy the module installation

Add the PsRepository of this gitlab repository

`Register-PSResourceRepository -Name RomisfragToolbox -Uri https://gitlab.com/api/v4/projects/38282722/packages/nuget/index.json -Trusted`

Now it should appear on the list when running the command :

`Get-PSResourceRepository`

Try to see if you can reach the module package 

`Find-PSResource -Name RomisfragPowershellToolbox`

And then you can install it

`Install-PSResource -Name RomisfragPowershellToolbox -Repository RomisfragToolbox`

That's it


# Update you version of the module

If you have already installed this module, then you should first remove all the cache of nuget if the release as just arrived

`nuget locals all -clear`

Then if you run `Find-PSResource -Name RomisfragPowershellToolbox` you should see the latest version of the module 

Install it 

`Install-PSResource -Name RomisfragPowershellToolbox -Repository RomisfragToolbox`

If with `Get-Command -Module RomisfragPowershellToolbox` you still see the old version, just run 

`Remove-Module PowershellGet` and you will now have the latest version only